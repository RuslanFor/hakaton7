
import { IAuthFields } from '@/app/components/layout/header/auth-form/auth-form.interface'
import { IAuthData } from '@/app/services/auth/auth.helper'
import { AuthService } from '@/app/services/auth/auth.service'
import { toastError } from '@/app/utils/apiutils'
import {createAsyncThunk} from '@reduxjs/toolkit'
import { toastr } from 'react-redux-toastr'


export const register = createAsyncThunk<IAuthData,IAuthFields>(
    'auth/register', async ({email, password}, thunkAPI) => {
        try {
            const response = await AuthService.register(email, password)
            toastr.success('Регистрация', 'Успешно выполнена')
            return response
        } catch (e) {
            
            toastError(e)
            return thunkAPI.rejectWithValue(e)
        }
    }
)




export const login = createAsyncThunk<IAuthData,IAuthFields>(
    'auth/login', async ({email, password}, thunkAPI) => {
        try {
            const response = await AuthService.login(email, password)
            toastr.success('Вход в систему', 'Успешно выполнен')
            return response
        } catch (e) {
            toastError(e)
            return thunkAPI.rejectWithValue(e)
        }
    }
)


export const logout = createAsyncThunk('auth/logout', async () => {
    return{}
})