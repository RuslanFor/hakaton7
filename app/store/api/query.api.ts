
import { IQuery, IQueryDto } from "@/app/types/query.interface";
import { api } from "./api";
import { QUERY } from "@/app/services/query.service";






export const videoApi = api.injectEndpoints({
    endpoints: builder => ({
        getVideosBySearchTerm: builder.query<IQuery[], string>({
            query: searchTerm => ({
                url:`/${QUERY}`,
                params: {searchTerm}
            }),
        }),
        getVideoById: builder.query<IQuery[], number>({
            query: id => ({
                url:`/${QUERY}/${id}`,
            }),
            providesTags: (result, error, id) => [{type: 'Video', id}]
        }),
        getVideoPrivate: builder.query<IQuery[], number>({
            query: id => ({
                url:`/${QUERY}/myquery/${id}`,
            }),
            providesTags: (result, error, id) => [{type: 'Video', id}]
        }),
        createVideo: builder.mutation<string, void>({
            query: () => ({
                url:`/${QUERY}`,
                method: 'POST'
            }),
            invalidatesTags: () => [{type: 'Profile'}]
        }),
        updateVideo: builder.mutation<IQuery, IQueryDto>({
            query: ({id, ...body}) => ({
                url:`/${QUERY}/${id}`,
                method: 'PUT',
                body
            }),
            invalidatesTags: (result,error, {id}) => [{type: 'Video', id}, {type:'Profile'}]
        }),
        getVideoMyID: builder.query<IQuery, any>({
            query: () => ({
                url:`/${QUERY}/myquery`,
                method: 'GET',
            }),
            providesTags: () => [{type: 'Video'}, {type:'Profile'}]
        }),
        updateViews: builder.mutation<IQuery, number>({
            query: id => ({
                url:`/${QUERY}/update-views/${id}`,
                method: 'PUT'
            }),
            invalidatesTags: (result,error, id) => [{type: 'Video', id}]
        }),
        updateLikes: builder.mutation<IQuery, number>({
            query: id => ({
                url:`/${QUERY}/update-likes/${id}`,
                method: 'PUT'
            }),
            invalidatesTags: (result,error, id) => [{type: 'Video', id}]
        }),
        deleteVideo: builder.mutation<void, number>({
            query: id => ({
                url:`/${QUERY}/${id}`,
                method: 'DELETE'
            }),
            invalidatesTags: () => [{type:'Video'},{type: 'Profile'}]
        }),
    })
})