import { IQuery } from '@/app/types/query.interface'
import { FC } from 'react'
import cn from 'classnames';
import styles from './VideoItem.module.scss';
import Image from 'next/image';
import Link from 'next/link';





const LargeVideoItem:FC<{video: IQuery}> = ({video}) => {
  return (
    <div className={cn(styles.video_item, styles.large_item)}>
        <div className={styles.thumbnail}>
            {video.thumbnailPath ?  (
                <Image
                    src={video.thumbnailPath}
                    alt={video.fullname}
                    layout='fill'
                    className={styles['bg-image']}
                    priority
                />
            ) : (<div>Ничего нет</div>)}
          

            <div className={styles.information}>
                <Link href={`/v/${video.id}`} legacyBehavior>
                    <a className={styles.name}>{video.fullname}</a>
                </Link>

                <div className={styles.author}>{video.user?.name}</div>

                
            </div>

        </div>
    </div>
  )
}

export default LargeVideoItem;