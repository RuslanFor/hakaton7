import { API_URL } from "@/app/api/axios"
import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/dist/query/react"
import { TypeRootState } from "../store"
import { IUser } from "@/app/types/user.interface"
import { USER } from "@/app/services/user.service"


export const api = createApi({
    reducerPath:'api',
    tagTypes:['Video', 'Profile'],
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        prepareHeaders: (headers, {getState}) => {
            const token = (getState() as TypeRootState).auth.accessToken
            if (token) headers.set('Authorization', `Bearer ${token}`)
            return headers
        }
    }),
    endpoints: builder =>({
        getProfile: builder.query<IUser, any>({
            query: () => `${USER}/profile`,
            providesTags: () => [{type: 'Profile'}]
        }),
        subscribeToChannel: builder.mutation<boolean, number>({
            query: (channelId) => ({
                url:`${USER}/subscribe/${channelId}`,
                method: 'PATCH'
            }),
            invalidatesTags: () => [{type:'Profile'}]
        }),
        
    })

})

