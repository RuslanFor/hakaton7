import React, { FC } from 'react'
import styles from '@/app/components/ui/Line.module.scss';
import Image from 'next/image';
import Link from 'next/link';

interface IVideoInformation{
    thumbnailPath?: string
    videoId: number
    fileName: string
    isUploaded:boolean
}




const VideoInformation: FC<IVideoInformation> = ({thumbnailPath,videoId,fileName,isUploaded}) => {
  return (
    <div className={styles.info}>
        {!thumbnailPath ? (
            <div className={styles.thumbnail}>
                {!isUploaded ? 'Идет загрузка видео...' : 'Ты должен загрузить превью'}
            </div>
        ) : (
            <Image
                src={thumbnailPath}
                width={344}
                height={190}
                alt={''}
                layout='responsive'
            />
        )}
        <div className={styles.details}>
            <div>
                <span>Video Link</span>
                <span>
                    <Link href={`/v/${videoId}`}>
                        <a>https://local/v/{videoId}</a>
                    </Link>
                </span>
            </div>
            <div>
                <span>FileName</span>
                <span>{fileName}</span>
            </div>
        </div>
    </div>
  )
}

export default VideoInformation