import { IBase } from "./base.interface"
import { IQuery } from "./query.interface"


export interface IUser extends IBase{
   
    email:string
    name: string
    isVerified?: boolean
    query?: IQuery[]

   
}


export interface ISubscription extends IBase{
    toChannel: IUser
   
}