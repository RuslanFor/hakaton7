import { IQuery } from "@/app/types/query.interface";



export interface IQueryItem{
    item: IQuery
    removeHandler?: (videoId: number) => void
    isUpdateLink?: boolean
    isSmall?: boolean
}