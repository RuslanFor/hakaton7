

import React, { FC, Fragment } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import styles from './UploadVideo.module.scss';

const UploadModal: FC<{ isOpen: boolean; setIsOpen: React.Dispatch<React.SetStateAction<boolean>>; videoId: string }> = ({ isOpen, setIsOpen, videoId }) => {
  const handleCloseModal = () => setIsOpen(false);

  return (
    <Transition show={isOpen} as={Fragment}>
      <Dialog onClose={handleCloseModal} className={styles.modal}>
        <Transition.Child
          as={Fragment}
          enter='ease-out duration-300'
          enterFrom='opacity-0'
          enterTo='opacity-100'
          leave='ease-in duration-200'
          leaveFrom='opacity-100'
          leaveTo='opacity-0'
        >
          <div className={styles.overlay} aria-hidden='true' />
        </Transition.Child>
        <div className={styles.wrapper}>
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0 scale-95'
            enterTo='opacity-100 scale-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100 scale-100'
            leaveTo='opacity-0 scale-95'
          >
            <Dialog.Panel className={styles.window}>
           
              <div>Your modal content goes here</div>
              <button onClick={handleCloseModal}>Close Modal</button>
            </Dialog.Panel>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );
};

export default UploadModal;