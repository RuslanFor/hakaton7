import { IQuery} from '@/app/types/query.interface'
import styles from './Catalog.module.scss';
import{ FC } from 'react'
import Heading from '@/app/components/ui/heading/Heading';
import VideoItem from '@/app/components/ui/query-item/QueryItem';

const Catalog:FC<{
  newQuery: IQuery[]
}> = ({newQuery}) => {
  return (
    <div className={styles.recommended}>
      <div className={styles.top_block}>
      </div>

      <div className={styles.catalog}>
        {newQuery.map(query => (
          <VideoItem
            item={query}   
            key={query.id}
               
          />
        ))}
      </div>
    </div>
  )
}

export default Catalog