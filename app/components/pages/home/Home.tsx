import { FC } from 'react'
import { IHome } from './home.interface'
import Layout from '../../layout/Layout'
import Search from './search/Search'
import './Home.css'

const Home: FC<IHome> = () => {
  return (
    <Layout title='Главная страница'>
      
        <div className='pt-40 flex items-center justify-center'>
          <div>
            <span className='text-4xl'>
            ПРОВЕРЬТЕ, ЕСТЬ ЛИ <br/> ВАШИ БЛИЗКИЕ В БАЗЕ И<br/> НЕ ИЩУТ ЛИ ВАС
            </span>
        
           <Search/>
           
           </div>
           
        </div>
        <div className='App'>
        
            <div className='new1'>
                <img src="new1.png" alt="Image1" width="300" height="200"/>
                <div className="newinp">
                <h3>Минобороны сообщило об уничтожении складов ВСУ в 5 областях Украины</h3>
                <p>Российские военные уничтожили склады ВСУ в Киевской, Днепропетровской, Житомирской, Харьковской и Одесской областях, сообщило Минобороны России.</p>
                </div>
            </div>
            <div className="new1">
            <img src="new2.png" alt="Image3" width="300" height="200"/>
            <div className="newinp">
                <h3>Сербия заявила о готовности участвовать в восстановлении Украины</h3>
                <p>Сербия хочет участвовать в восстановлении Украины в рамках совместной инициативы с США, заявил министр строительства, транспорта и инфраструктуры страны Горан Весич</p>
            </div>
            </div>
            <div className="new1">
            <img src="new3.png" alt="Image3" width="300" height="200"/>
            <div className="newinp">
                <h3>ВСУ сообщили о повреждении энергетического объекта в Одесской области.</h3>
                <p>В Одесском районе при ударе по объекту энергетической инфраструктуры произошел пожар, есть повреждения, сообщили в ВСУ.</p>
            </div>
            </div>
        </div>
        
            
      
        
    </Layout>
  )
}

export default Home