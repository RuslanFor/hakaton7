import { FC, forwardRef } from 'react'
import styles from './Fields.module.scss'
import { ITextArea } from './text-area.interface'



const TextArea = forwardRef<HTMLTextAreaElement, ITextArea>(
    ({ error, style, ...rest}, ref) => {
        return(
            <div className={styles.input} style={style}>
                <textarea ref={ref} {...rest}/>
                {error && <div className={styles.error}>{error.message}</div>}
            </div>
        )
    }
)


TextArea.displayName = 'TextArea'


export default TextArea;