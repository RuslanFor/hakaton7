import { FC } from 'react'
import { useSearch } from './useSearch'
import styles from './Search.module.scss'
import VideoItem from '@/app/components/ui/query-item/QueryItem'

const Search:FC = () => {
  const {data, handleSearch, searchTerm, isSuccess } = useSearch()
  return (
    <div className={styles.search_top}>
      <label>
        <input 
          type='text'
          placeholder='Введите...'
          value={searchTerm}
          onChange={handleSearch}  
        />
        <img src='/img/common/search.svg' alt=''/>
      </label>
      {isSuccess && (
        <div className={styles.result}>
          {data?.length ? (
            data.map(query => <VideoItem isSmall item={query} key={query.id}/>)
            ) : (
              <div className='text-black'> не найдено</div>
            )}
        </div>
      )}
    </div>
  )
}

export default Search