import Layout from '@/app/components/layout/Layout'
import { IHome } from '@/app/components/pages/home/home.interface'
import Query from '@/app/components/pages/query/Query'
import { useAuth } from '@/app/hooks/useAuth'
import { NextPageAuth } from '@/app/providers/private-route.interface'
import { VideoService } from '@/app/services/query.service'
import { api } from '@/app/store/api/api'
import { videoApi } from '@/app/store/api/query.api'
import { GetStaticProps } from 'next'
import React from 'react'



const QueryPage:NextPageAuth<IHome> = (props) => {
    
  return (
    <Query/>
  )
}


QueryPage.isOnlyUser = true





export default QueryPage

