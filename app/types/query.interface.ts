import { IBase } from "./base.interface"
import { IUser } from "./user.interface"


export interface IQuery extends IBase{
    fullname: string
    status:string
    isPublic: boolean
    description:string
    thumbnailPath:string
    user: IUser
    
   
}


export interface IQueryDto extends Pick<IQuery, 'id' | 'thumbnailPath' | 'description' | 'fullname'  | 'isPublic'>{}