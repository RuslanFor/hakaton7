
import { VideoService } from '@/app/services/query.service';
import { GetStaticProps, NextPage } from 'next';
import { IHome } from '@/app/components/pages/home/home.interface';
import NotWefound from '@/app/components/pages/notwefound/NotWefound';


const NotWefoundPage: NextPage<IHome> = (props) => {
  return (
    <NotWefound {...props} />
  )
}




export const getStaticProps: GetStaticProps = async () => {
  try{
    const {data:newQuery} = await VideoService.getAllPublicNotWefound()
    return {
      props: {
        newQuery,
      } as IHome
    }
  }catch (e){
    return {
      props: {
        newQuery: [],
      
      } as IHome
    }
  }
}



export default NotWefoundPage;

