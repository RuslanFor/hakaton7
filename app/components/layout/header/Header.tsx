import { FC } from 'react'
import styles from './Header.module.scss'
import IconsRight from './icons-right/IconsRight'
import Link from 'next/link'
import Image from 'next/image'


const Header: FC = () => {
  return (
    <header className={styles.header}>
      <div className='logo'> 
        <Link href='/'>
          <Image src="/pngwing.com.png" alt="logo" width={50} height={50} />
          

        </Link>
      </div>
      <div className='found'>
        <div>
            <a href='wefound'>Уже нашли</a>
        </div>
      </div>
      <div className='notfound'>
        <div>
            <a href='founding'>Еще ищем</a>
        </div>
      </div>
      <div className='sign'>
          <IconsRight/>
      </div>
      
    </header>
  )
}

export default Header