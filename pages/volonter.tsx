
import UploadModal from '@/app/components/pages/query/query-form/UploadModal'
import UploadVideo from '@/app/components/pages/query/query-form/UploadVideo'
import Volonter from '@/app/components/pages/volonter/Volonter'
import CoursesList from '@/app/components/ui/curses/courses-list'
import { NextPageAuth } from '@/app/providers/private-route.interface'
import React from 'react'

const VolonterPage: NextPageAuth = () => {
  return (
    <Volonter/>
  )
}


VolonterPage.isOnlyUser = true

export default VolonterPage;