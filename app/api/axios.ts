import axios from 'axios'
import { getContentType } from '../utils/apiutils'

export const API_URL = `http://localhost:3000/api`

export const axiosClassic = axios.create({
    baseURL: API_URL,
    headers: getContentType()
})