import React, { FC } from 'react'
import Layout from '../../layout/Layout'
import Catalog from '../home/catalog/Catalog'
import { IHome } from '../home/home.interface'

const Wefound:FC<IHome> = ({newQuery}) => {
   
  return (
    <Layout title='Найденные'>
        <Catalog newQuery={newQuery}/>
    </Layout>
  )
}

export default Wefound