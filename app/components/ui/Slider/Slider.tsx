import { useEffect, useState, Children, cloneElement } from 'react';
import './Slider.css'
import {FaChevronLeft, FaChevronRight} from 'react-icons/fa'
const WIDTH = 600
 const Slider = ({children}) =>{

    const handleLeftArrowClick=()=>{
        setoffset((currentOffset)=>{
            const newOffset = currentOffset +  WIDTH
            return Math.min(newOffset, 0)
        })
    }
    const handleRightArrowClick=()=>{
        setoffset((currentOffset)=>{
            const newOffset = currentOffset - WIDTH
            const maxoffset =-( WIDTH * (pages.length-1))
            return Math.max(newOffset, maxoffset)
        })
    }

    const [pages, setPages] = useState([])
    const [offset, setoffset]= useState(0)
    useEffect(() => {
        setPages(
            Children.map(children, child => {
                return cloneElement(child, {
                    style:{
                        height: '100%',
                        minWidth:`${WIDTH}px`,
                        maxWidth: `${WIDTH}px`,
                    },
                })
            })
        )
    }, [])
    return(
        <div className='mainCont'>
            <FaChevronLeft className="arrow" onClick={handleLeftArrowClick}></FaChevronLeft>
            <div className='window'>
                <div className='allItems'
                style={{transform: `translateX(${offset}px)`}}>
                    {pages}
                </div>
            </div>
            <FaChevronRight className="arrow" onClick={handleRightArrowClick}></FaChevronRight>

        </div>
    );
}


export default Slider;