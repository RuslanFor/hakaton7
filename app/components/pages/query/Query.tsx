import React, { FC } from 'react'
import Layout from '../../layout/Layout'
import Catalog from '../home/catalog/Catalog'
import { api } from '@/app/store/api/api'
import Loader from '../../ui/loader/Loader'

const Query: FC = () => {
    const {data, isLoading} = api.useGetProfileQuery(null)
    const querys = data?.query
  return (
    <Layout title='Мои заявки'>
        <div>
        {isLoading ? (
            <Loader count={5}/>
            ) : querys?.length ? (
                <Catalog
                    newQuery={querys}
                  
                />
            ) : (
                <p></p>
            )}
          
        </div>
    </Layout>
  )
}

export default Query