import { axiosClassic } from "../api/axios"
import { IQuery } from "../types/query.interface"


export const QUERY = 'query'

export const VideoService = {
    async getAllPublic(){
        return axiosClassic.get<IQuery[]>(`/${QUERY}`)
    },
    async getAllPublicNotWefound(){
        return axiosClassic.get<IQuery[]>(`/${QUERY}/notwefound`)
    },
    async getAllPublicWefound(){
        return axiosClassic.get<IQuery[]>(`/${QUERY}/wefound`)
    },
    async getMyquery(){
        return axiosClassic.get<IQuery[]>(`/${QUERY}/myquery`)
    }
}