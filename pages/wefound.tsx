import Catalog from '@/app/components/pages/home/catalog/Catalog';
import { VideoService } from '@/app/services/query.service';
import { GetStaticProps, NextPage } from 'next';
import React, { FC } from 'react'
import shuffle from 'lodash/shuffle'
import { IQuery } from '@/app/types/query.interface';
import { IHome } from '@/app/components/pages/home/home.interface';
import Home from '@/app/components/pages/home/Home';
import Wefound from '@/app/components/pages/wefound/Wefound';

const WefoundPage: NextPage<IHome> = (props) => {
  return (
    <Wefound {...props}/>
  )
}




export const getStaticProps: GetStaticProps = async () => {
  try{
    const {data:newQuery} = await VideoService.getAllPublicWefound()
    return {
      props: {
        newQuery,
      } as IHome
    }
  }catch (e){
    return {
      props: {
        newQuery: [],
      
      } as IHome
    }
  }
}



export default WefoundPage;

