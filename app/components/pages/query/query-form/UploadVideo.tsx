import { videoApi } from '@/app/store/api/query.api'
import React, { FC, useState } from 'react'
import UploadModal from './UploadModal'
import Button from '@/app/components/ui/button/Button'

const UploadVideo:FC = () => {
  const [isOpen, setIsOpen ] = useState(false)  
  const [video, setVideoId] = useState<number>(0)

  const [createVideo, {isLoading}] = videoApi.useCreateVideoMutation()

  return (
    <>
     <Button
      className="l"
      disabled={isLoading}
      onClick={() =>{
        createVideo()
          .unwrap()
          .then(id => {
            setVideoId(+id)
            setIsOpen(true)
          })
      
      }}
      >dfdfdf
     </Button>
     
     <UploadModal isOpen={isOpen} setIsOpen={setIsOpen} videoId={video}/>
    </>
  )



}
export default UploadVideo;