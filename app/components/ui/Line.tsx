import React from 'react'
import styles from '@/app/components/ui/Line.module.scss';

const Line = () => {
  return (
    <div className={styles.line}/>
  )
}

export default Line