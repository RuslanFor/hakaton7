import Image from 'next/image';
import  Slider  from '../Slider/Slider';
import './Human.css'
import Layout from '../../layout/Layout';


function Human() {
    return(
    <Layout title='Иван Иванов Иванович'>
        <div className='humanPage'>
            <div className='demoinfo'>
            <Image src='/gg.jpg'  alt="ffwfd" width={200} height={300}/>
    
                <p >Дата Рождения:</p>
                <div className='birth'>
                    <p>14.08.2001</p>
                </div>
                <p >Когда Найден:</p>
                <div className='birth'>
                    <p className='birth'>12.11.2023</p>
                </div>
            </div>
            <div className='maininfo'>
                <div>
                    <h1>Иванов Иван Иванович</h1>
                </div>
                <div>
                    <h3>Статус: </h3>
                    <p>Найден, жив</p>
                </div>
                <div>
                    <h3>
                        Основная информация:
                    </h3>
                    <div>
                    <Slider className='items'>
                        <Image src='/new3.png'  alt="ffwfd" width={500} height={350} className='slider1'/>
                        <Image src='/new1.png'  alt="cwdcwc" width={500} height={350} className='slider2'/>
                        <Image src='/new2.png'  alt="ffwsddfd" width={500} height={350} className='slider3'/>

                     </Slider>
                    </div>
                    <div>
                        <p>
                        Потерялся
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </Layout>
        
    );
    

}
export default Human;