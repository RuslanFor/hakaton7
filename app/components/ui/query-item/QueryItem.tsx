
import { FC } from 'react'
import { IQueryItem} from './query-item.interface'
import { useRouter } from 'next/router'
import styles from './QueryItem.module.scss'
import cn from 'classnames'
import {BiEdit, BiTrash} from 'react-icons/bi'
import Image from 'next/image'
import Link from 'next/link'


const VideoItem: FC<IQueryItem> = ({isSmall, isUpdateLink, removeHandler,item}) => {
    const {push} = useRouter()
    return (
        <div className={cn(styles.video_item, {
            [styles.small]: isSmall
            })}
        >
            {!!removeHandler && (
                <button className={'absolute bottom-3 right-3 z-10'} onClick={() => removeHandler(item.id)}>
                    <BiTrash className='text-lg text-red-700' />
                </button>
            )}
            {isUpdateLink && (
                <button className={'absolute bottom-3 right-11 z-10'} onClick={() => push(`/query/edit/${item.id}`)}>
                    <BiEdit className='text-lg text-blue-600'/>
                </button>
            )}

            <div className={styles.thumbnail}>
                
                    <Image
                        src='/gg.jpg'
                        alt={item.fullname}
                        width={200}
                        height={200}
                    
                        priority
                    />
                
               
            </div>

            <div className={styles.information}>
                <Link href={`/v/${item.id}`} legacyBehavior>
                    <a className={styles.name}>{item.fullname}</a>
                </Link>
                <div className=' text-black  text-sm'>{item.description}</div>
                <div className=' text-black pt-10'>Статус: {item.status}</div>
                
            </div>
            
        </div>
    )
}

export default VideoItem