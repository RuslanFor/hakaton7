import React, { FC } from 'react'
import useUploadQueryForm from './useUploadQueryForm'
import Field from '@/app/components/ui/field/Field'
import TextArea from '@/app/components/ui/text-area/TextArea'
import { Controller } from 'react-hook-form'
import TogglePublic from './TogglePublic'
import { IMediaResponse } from '@/app/services/media/media.interface'
import UploadField from '@/app/components/ui/upload-field/UploadField'
import VideoInformation from './VideoInformation'
import styles from './UploadVideo.module.scss'

const UploadQueryForm:FC<{videoId:number, handleCloseModal: () => void}>= ({videoId, handleCloseModal}) => {
  const {form, status, media} = useUploadQueryForm({videoId,handleCloseModal})
  return(
    <form onSubmit={form.handleSubmit(form.onSubmit)} className=' flex flex-wrap'>
      {status.isSuccess && <div>Ура!</div>}
      {status.isChosen ? (
        <>
          <div className='w-7/12 pr-6 pt-3'>
            <Field 
              {...form.register('fullname', {
                required: 'ФИО обязательно!'
              })}
              placeholder='ФИО'
              error={form.errors.fullname}
            
            />
            <TextArea
             {...form.register('description', {
              required: 'Описвание обязательно!'
            })}
            placeholder='Описание...'
            error={form.errors.description}
          
            />
          <div className='mt-8'>
            <Controller
            control={form.control}
            name='thumbnailPath'
            render={({field: {onChange}}) => (
              <UploadField
              folder='thumbnails'
              onChange={(value: IMediaResponse) => {
                onChange(value.url)
              }}
              />
            )}
          />
          </div>
          <Controller 
          control={form.control}
          name='isPublic'
          render={({field: {onChange, value}}) => (
            <TogglePublic
            clickHandler={() => {
              onChange(!value)
            }}
            isEnabled={!!value}
            />
          )}
          />
          </div>
          <div className={'w-5/12 p-3 pl-10'}>
            <VideoInformation 
              fileName={media.videoFileName}
              videoId={videoId}
              isUploaded={status.isUploaded}
              thumbnailPath={media.thumbnailPath}
            />
          </div>

        </>
      ) : (
      <div className={styles.uploadScreen}>
        <Controller
            control={form.control}
            name='thumbnailPath'
            render={({field: {onChange}}) => (
              <UploadField
              title={'Грузи!'}
              folder='thumbnails'
              onChange={media.handleUploadVideo}
              setIsChosen={status.setIsChosen}
              />
            )}
          />
      </div>
      )}
    </form>
  )
}

export default UploadQueryForm