import { IMediaResponse } from '@/app/services/media/media.interface'
import { videoApi } from '@/app/store/api/query.api'
import { IQueryDto } from '@/app/types/query.interface'
import React, { useState } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'





interface IUseUploadQueryForm{
    handleCloseModal: () => void
    videoId: number
}






export const useUploadQueryForm = ({handleCloseModal, videoId}:IUseUploadQueryForm) => {
    const {
        register,
        formState: {errors},
        control,
        handleSubmit,
        watch,
        setValue,
        reset
    } = useForm<IQueryDto>({
        mode:'onChange'
    })

    const [updateVideo, {isSuccess}] = videoApi.useUpdateVideoMutation()

    const onSubmit: SubmitHandler<IQueryDto> = data => {
        updateVideo({...data, id: videoId})
            .unwrap()
            .then(() => {
                handleCloseModal()
                reset()
            })
    }


    const thumbnailPath = watch('thumbnailPath')
    const [videoFileName, setVideoFileName] = useState('')

    const handleUploadVideo = (value: IMediaResponse) => {
        setValue('fullname', value.name)
        setVideoFileName(value.name) 

        
    }
        const [isChosen,setIsChosen] = useState(false)
        const [percent, setPercent] = useState(0)
        const [isUploaded, setIsUploaded] = useState(false)
  return{ 
    form: {
        register,
        errors,
        control,
        handleSubmit,
        onSubmit
    },
    media: {
        thumbnailPath,
        videoFileName,
        handleUploadVideo,
    },
    status:{
        isSuccess,
        isChosen,
        setIsChosen,
        percent,
        isUploaded,

    }
  }
}

export default useUploadQueryForm