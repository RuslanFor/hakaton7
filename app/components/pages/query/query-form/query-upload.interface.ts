import { Dispatch, SetStateAction } from "react"



export interface IUploadMedia{
    isOpen: boolean
    setIsOpen: Dispatch<SetStateAction<boolean>>
    queryId: number
    
}