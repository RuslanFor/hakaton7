import React, { FC } from 'react'
import Layout from '../../layout/Layout'
import Catalog from '../home/catalog/Catalog'
import { IHome } from '../home/home.interface'

const NotWefound:FC<IHome> = ({newQuery}) => {
  return (
    <Layout title='Не найденные'>
        <Catalog newQuery={newQuery}/>
    </Layout>
  )
}

export default NotWefound