import { useActions } from '@/app/hooks/useActions'
import { useAuth } from '@/app/hooks/useAuth'
import { useOutside } from '@/app/hooks/useOutside'
import { api } from '@/app/store/api/api'
import { FC } from 'react'
import styles from './ProfileMenu.module.scss'
import Image from 'next/image'
import {GoChevronDown,GoChevronUp} from 'react-icons/go'
import Link from 'next/link'

const ProfileMenu:FC = () => {

  const {user} = useAuth()
  const {data, isLoading} = api.useGetProfileQuery(null, {
    skip: !user
  })

  const {isShow, setIsShow, ref} = useOutside(false)
  const {logout} = useActions()

  if (isLoading) return null
  return (
    <div ref={ref} className={styles.wrapper}>
      <button onClick={() => setIsShow(!isShow)}>
        <Image
          src='/avatar.png'
          alt={''}
          width={40}
          height={40}
          priority
        />
        <span className={styles.name}>{data?.name}</span>
        {isShow ? <GoChevronUp/> : <GoChevronDown/>}
      </button>
      {isShow && (
        <div className={styles['profile-menu']}>
            <ul>
              
              <li>
                <Link href='/query'>
                    <div>Оставить заяку</div>
                </Link>
                
              </li>
              <li>
                <Link href='/volonter'>
                    <div>Я волонтер!</div>
                </Link>
                
              </li>
              <li>
                <button onClick={logout}>Выйти</button>
              </li>
            </ul>
        </div>
      )}
    </div>
  )
}

export default ProfileMenu